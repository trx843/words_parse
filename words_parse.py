import re
import os
import time
from datetime import datetime 

directory = 'd:\\!!!Audit\\src'
dict_file = 'd:\\opt\\keywords_search\\dict_file01.txt'
output_file = 'd:\\!!!Audit\\result_lozung.txt'


def clear_line(str_in):
    if str_in:
        # удаляем пробелы
        str_in = str_in.strip()
        # удаляем каретки
        str_in = re.sub(r'(^\s+|\n|\r|\s+$)', '', str_in)
        # переводим в lower
        str_in = str_in.lower()
    return str_in
 
def prepare_line(str_in):
    if str_in:
        # удаляем пробелы
        str_in = str_in.strip()
        # удаляем каретки
        str_in = re.sub(r'(^\s+|\n|\r|\s+$)', '', str_in)
        # переводим в lower
        str_in = str_in.lower()
        str_in = str_in.replace('#', ' ')
        print(str_in)
    return str_in 

def countfiles(dir):
    count = 0
    for root, dirs, files in os.walk(dir):
        count += len(files)
    return count
 

def read_from_dict(name_file_dict):
    words_dict = []
    with open(name_file_dict, 'r', encoding='utf-8') as d_file:
        try:
            # очищаем от  пробелов и конца строки
            #words_dict = [clear_line(line) for line in d_file]
            words_dict = [prepare_line(line) for line in d_file]
            # очищаем список от пустых элементов
            words_dict = list(filter(None, words_dict))
        except:
            print('Error, что то не то с файлом словаря')
            d_file.close()
        finally:
            d_file.close()
    return words_dict
 

def search_words(p_directory, p_words, p_output_file):
    with open(p_output_file, 'w', encoding='utf-8') as file_out:
        kol_read_file = 0
        count_matches = 0
        for dirpath, dirnames, filenames in os.walk(p_directory):
            for filename in filenames:
                path_to_file = os.path.join(dirpath, filename)
                with open(path_to_file, 'r', encoding='utf-8') as file_src:
                    try:
                        content = file_src.read()
                        content = content.lower()
                        for word in words:
                            matches = re.finditer(word, content)
                            if matches:
                                for matchNum, match in enumerate(matches, start=1):
                                    count_matches = count_matches + 1
                                    #print(f'Вхождение:{word}\nФайл:{path_to_file}\nПозиция:{match.start()}-{match.end()}\nСтрока:\n{match.string[match.start() - 50:match.end() + 50]}\n\n')
                                    file_out.write(f'Вхождение:{word}\nФайл:{path_to_file}\nПозиция:{match.start()}-{match.end()}\nСтрока:\n{match.string[match.start() - 50:match.end() + 50]}\n\n')
                    except:
                        pass

                kol_read_file = kol_read_file + 1
                proc_read_file = round((kol_read_file * 100) / count_files, 1)
                print(f'\rПросмотрено файлов: [{kol_read_file}] из [{count_files}] [{proc_read_file}%] Совпадений [{count_matches}]', end='')

        if  count_matches == 0:
            file_out.write('Лозунги из словаря не обнаружены...')

    file_out.close()

if __name__ == '__main__':
    # запускаемся
    current_datetime_bof = datetime.now()
    print('*********************************************************')
    print(f'Начало обработки...  {current_datetime_bof}')
    print('*********************************************************')

    #print(f'Подсчитаем количество объектов в каталоге: {directory}')
    count_files = countfiles(directory)
    #print(f'Обнаружено файлов:  {count_files} ')

    words = read_from_dict(dict_file)
    print(f'Словарь:{words}')

    search_words(directory, words, output_file) 

    current_datetime_eof = datetime.now()

    print('\n*********************************************************')
    print(f"Обработка закончена... {current_datetime_eof}")
    print(f"Общее время........... {current_datetime_eof - current_datetime_bof}")
    print('*********************************************************')